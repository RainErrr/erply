#Assigment

Implement login and form page, include inline validations and global alerts either on error or on success + make various API calls to auth, ask for form field selections and save/load form data.

##Installation

* Make sure you have at least node 15 and relevant npm
* `git clone <project>` to clone project
* `npm i` for node modules

##Development:

###Run UI
`npm run dev`

###Test
`npm run test`

##Stack
* Typescript
* ReactJS
* Vite
* Bootstrap 5