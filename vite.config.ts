import { defineConfig } from 'vite'
import reactRefresh from '@vitejs/plugin-react-refresh'
import svgr from 'vite-plugin-svgr'

export default defineConfig({
  plugins: [reactRefresh(), svgr()],
  build: {
    outDir: 'build'
  },
  resolve: {
    alias: {
      '~bootstrap': 'bootstrap'
    }
  },
  server: {
    proxy: {
      '/api/erply': {
        target: 'https://520340.erply.com/api',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api\/erply/, '')
      },
      '/api/cafa': {
        target: 'https://api-cafa-us.erply.com',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api\/cafa/, '')
      }
    }
  }
})
