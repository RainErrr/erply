import StatusCodes from 'http-status-codes'

const {NO_CONTENT} = StatusCodes

class Api {
  async post(url: string, data: object) {
    return await this.request(url, data, 'POST')
  }

  async cafaPost(url: string, data: object, jwt: string) {
    return await this.cafaRequest(url, data, 'POST', jwt)
  }

  async cafaGet(url: string, data: object, jwt: string) {
    return await this.cafaRequest(url, data, 'GET', jwt)
  }

  private async request(url: string, data: any, methodName: 'POST') {
    const payload: RequestInit = {
      method: methodName,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }

    const formBody = []
    for (let property in data) {
      const encodedKey = encodeURIComponent(property)
      const encodedValue = encodeURIComponent(data[property])
      formBody.push(encodedKey + "=" + encodedValue)
    }

    payload.body = formBody.join("&")
    return await fetch(url, payload)
      .then(response => Api.parseResponse(response))
  }

  private async cafaRequest(url: string, data: object, methodName: 'POST' | 'GET', jwt: string) {
    const payload: RequestInit = {
      method: methodName,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'jwt': jwt,
      }
    }

    if (methodName === 'GET') {
      const formBody = []
      for (let property in data) {
        const encodedKey = encodeURIComponent(property)
        const encodedValue = encodeURIComponent((data as any)[property])
        formBody.push(encodedKey + "=" + encodedValue)
      }

      url = url + '?' + formBody.join("&")
    }
    if (methodName !== 'GET') payload.body = JSON.stringify(data)
    return await fetch(url, payload)
      .then(response => Api.parseResponse(response))
  }

  private static async parseResponse(response: Response): Promise<any> {
    const json = response.status === NO_CONTENT ? {} : await response.json()
    if (response.ok) {
      return json
    } else {
      return Promise.reject({status: response.status, ...json})
    }
  }
}

export default new Api()