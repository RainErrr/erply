import ObjectID from 'bson-objectid'
import {useContext} from 'react'
import {Alert, AlertContext, AlertType} from '../providers/AlertProvider'

interface AlertState {
  alertContents: Alert[]
  renderSuccessAlert: (content: string) => void
  renderErrorAlert: (content: string) => void
  removeAlerts: () => void
}

const useAlert = (): AlertState => {
  const {alertContents, setAlertContents} = useContext(AlertContext)

  const renderSuccessAlert = (content: string) => {
    const alert = {content, type: AlertType.SUCCESS, id: new ObjectID().toString()}
    setAlertContents(() => [alert])
  }

  const renderErrorAlert = (content: string) => {
    const alert = {content, type: AlertType.DANGER, id: new ObjectID().toString()}
    setAlertContents(() => [alert])
  }

  const removeAlerts = () => setAlertContents(() => [])

  return {alertContents, renderSuccessAlert, renderErrorAlert, removeAlerts: removeAlerts}
}

export default useAlert