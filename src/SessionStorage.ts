class SessionStorage {
  private sessionKey: string | null = null
  private jwtToken: string | null = null

  setSessionKey(sessionKey: string | null) {
    this.sessionKey = sessionKey
  }

  getSessionKey() {
    return this.sessionKey || ''
  }

  setJwtToken (jwtToken: string | null) {
    this.jwtToken = jwtToken
  }

  getJwtToken() {
    return this.jwtToken || ''
  }
}

const sessionStorage = new SessionStorage()
export default sessionStorage