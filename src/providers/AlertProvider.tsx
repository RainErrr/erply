import React, {ReactNode, useState} from 'react'

export const AlertContext = React.createContext({} as any)

export enum AlertType {
  SUCCESS = 'SUCCESS',
  DANGER = 'DANGER'
}

export interface Alert {
  id: string
  content: string
  type: AlertType
}

const AlertProvider = ({children}: { children: ReactNode }) => {
  const [alertContents, setAlertContents] = useState<Alert[]>([])

  return (
    <AlertContext.Provider value={{alertContents, setAlertContents}}>
      {children}
    </AlertContext.Provider>
  )
}

export default AlertProvider