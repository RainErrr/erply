import React, {ReactElement, ReactNode} from 'react'

type ProviderType = ({children}: { children: ReactNode | ReactNode[] }) => ReactElement

export function withProviders<T>(providers: ProviderType[], Component: React.FC<T>) {
  return (props: T) => {
    return providers.reduce((Result, Provider) => {
      return <Provider>{Result}</Provider>
    }, <Component {...props} />)
  }
}