import React, {ReactNode, useState} from 'react'
import sessionStorage from '../SessionStorage'

export const UserContext = React.createContext({} as any)

export interface UserData {
  userID: string,
  userName: string,
  clientCode: string
}

interface Records {
  userID: string,
  userName: string
  sessionKey: string,
  token: string
}

const UserProvider = ({children}: { children: ReactNode }) => {
  const [user, setUser] = useState<UserData|null>(null)

  const setUserCredentials = (records: Records, clientCode: string) => {
    const payload = {
      userID: records.userID,
      userName: records.userName,
      clientCode
    } as UserData
    setUser(payload)
    sessionStorage.setSessionKey(records.sessionKey)
    sessionStorage.setJwtToken(records.token)
  }

  const logoutUser = () => {
    setUser(null)
    sessionStorage.setSessionKey(null)
    sessionStorage.setJwtToken(null)
  }

  const state = {
    user,
    logoutUser,
    setUserCredentials,
  }

  return (
    <UserContext.Provider value={state}>
      {children}
    </UserContext.Provider>
  )
}

export default UserProvider