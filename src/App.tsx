import React, {ReactElement, useContext} from 'react'
import {BrowserRouter as Router, Redirect, Route, Switch} from 'react-router-dom'
import Navbar from './components/Navbar'
import Login from './components/Login'
import ConfigurationForm from './components/ConficurationForm'
import Alerts from './components/utils/Alerts'
import {withProviders} from './providers/utils'
import AlertProvider from './providers/AlertProvider'
import UserProvider, {UserContext} from './providers/UserProvider'

const App: React.FC = (): ReactElement => {
  const {user} = useContext(UserContext)

  const authorized = (
    <>
      <Router>
        <Navbar/>
        <Switch>
          <Route path={"/"} component={ConfigurationForm}/>
          <Route path="/login" render={() => <Redirect to="/"/>}/>
        </Switch>
      </Router>
      <Alerts/>
    </>
  )

  const unAuthorized = (
    <>
      <Router>
        <Navbar/>
        <Switch>
          <Route path={"/login"} component={Login}/>
          <Route path={"/"} render={() => <Redirect to="/login"/>}/>
        </Switch>
      </Router>
      <Alerts/>
    </>
  )

  return <>{user ? authorized : unAuthorized}</>
}

export default withProviders([AlertProvider, UserProvider], App)