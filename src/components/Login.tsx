import React, {ReactElement, useContext, useEffect, useState} from 'react'
import {useHistory} from 'react-router-dom'
import useAlert from '../hooks/useAlert'
import {login} from '../client-api/erplyApi'
import {UserContext} from '../providers/UserProvider'

export interface Credentials {
  username: string,
  password: string,
  clientCode: string
}

const Login: React.FC = (): ReactElement => {
  const history = useHistory()
  const {removeAlerts} = useAlert()
  const {setUserCredentials} = useContext(UserContext)
  const {renderErrorAlert} = useAlert()
  const [credentials, setCredentials] = useState<Credentials>({} as Credentials)
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    removeAlerts()
  }, [])


  const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    event.preventDefault()
    const {name, value} = event.target
    setCredentials(credentials => ({...credentials, [name]: value}))
  }

  const onSubmit = (event: React.MouseEvent<HTMLButtonElement> | React.FormEvent) => {
    event.preventDefault()
    removeAlerts()
    setLoading(true)

    login(credentials).then(response => {
      setUserCredentials(response.records[0], credentials.clientCode)
      history.push('/')
    }).catch(error => {
      console.log(error)
      renderErrorAlert('Oops! Something went wrong. Please review the form.')
      setLoading(false)
    })
  }

  return (
    <form onSubmit={onSubmit}>
      <div className="login d-flex justify-content-center">
        <div className="d-flex flex-column">
          <div className="d-flex justify-content-center">
            <div className="title">Login to Erply</div>
          </div>
          <input name="clientCode" type="text" placeholder="Insert Account Number" onChange={onChange} required/>
          <input name="username" type="text" placeholder="Insert Email" onChange={onChange} required/>
          <input name="password" type="password" placeholder="Insert Password" onChange={onChange} required/>
          <div className="d-flex justify-content-center">
            <button className="btn" type="submit" disabled={loading}>
              {loading ?
                <>
                  <span className="spinner-border spinner-border-sm"/> Loading...
                </>
                : 'Login'}
            </button>
          </div>
        </div>
      </div>
    </form>
  )
}

export default Login