import React, {ReactElement, useContext, useEffect, useState} from 'react'
import Input from './utils/Input'
import Select from './utils/Select'
import useAlert from '../hooks/useAlert'
import {getPaymentTypes, getWarehouses, RecordArray} from '../client-api/erplyApi'
import {UserContext} from '../providers/UserProvider'
import {getConfiguration, saveConfiguration} from '../client-api/cafaApi'
import {useForm} from 'react-hook-form'
import {isValidEmail, isValidUrl} from './Validation'

interface Configuration {
  name: string,
  url: string,
  warehouse: string,
  email: string,
  paymentGateway: string,
  paymentType: string
}

const ConfigurationForm: React.FC = (): ReactElement => {
  const {user} = useContext(UserContext)
  const {renderErrorAlert, renderSuccessAlert, removeAlerts} = useAlert()
  const [wareHouses, setWareHouses] = useState<RecordArray[]>([])
  const [paymentType, setPaymentType] = useState<RecordArray[]>([])
  const [loading, setLoading] = useState(false)
  const [configuration, setConfiguration] = useState<Configuration | null>(null)
  const {register, handleSubmit, formState: {errors}} = useForm()

  useEffect(() => {
    getWarehouses(user).then(response => setWareHouses(response.records))
    getPaymentTypes(user).then(response => setPaymentType(response.records))
    getConfiguration().then(response => setConfiguration(response[0]?.value || {}))
  }, [])

  const onChange = (event: React.ChangeEvent<HTMLInputElement> | React.ChangeEvent<HTMLSelectElement>) => {
    event.preventDefault()

    const {name, value} = event.target
    setConfiguration(form => {
      if (!form) form = {} as Configuration
      return ({...form, [name]: value})
    })
  }

  const onSave = () => {
    setLoading(true)
    removeAlerts()

    saveConfiguration(configuration).then(response => {
      if (response.statusCode === '200' || response.statusCode === '201') {
        renderSuccessAlert('Thank you! Your settings have been saved.')
        setLoading(false)
      }
    }).catch(error => {
      console.log(error)
      renderErrorAlert('Oops! Something went wrong. Please review the form.')
      setLoading(false)
    })
  }

  const onErrors = () => renderErrorAlert('Oops! Something went wrong. Please review the form.')

  return (
    <form className="configuration">
      {configuration ?
        <div className="container-fluid d-flex justify-content-center">
          <div className="d-flex flex-column">
            <div className="title title-basic">Basic Information</div>
            <div className="d-flex">
              <div className="input-group">
                <Input {...register('name', {required: 'Field is empty'})}
                       type="text"
                       placeholder="Insert Company Name"
                       value={configuration?.name || ''}
                       onChange={onChange}
                       error={errors.name && errors.name.message}/>
              </div>
              <div className="input-group spacer">
                <Input {...register('url', {validate: url => isValidUrl(url) || 'Url is not valid'})}
                       type="text"
                       placeholder="Insert Website URL"
                       value={configuration?.url || ''}
                       onChange={onChange}
                       error={errors.url && errors.url.message}/>
              </div>
            </div>
            <div className="d-flex">
              <div className="input-group">
                <Select {...register('warehouse', {required: 'Nothing selected'})}
                        className="form-select"
                        value={configuration?.warehouse || ''}
                        onChange={onChange}
                        error={errors.warehouse && errors.warehouse.message}
                        required>
                  <option className="text-muted" value="">Select Warehouse</option>
                  {wareHouses && wareHouses.map((warehouse, i) => {
                    return (
                      <option key={warehouse.name + i} value={warehouse.name}>{warehouse.name}</option>
                    )
                  })}
                </Select>
              </div>
              <div className="input-group spacer">
                <Input {...register('email', {validate: email => isValidEmail(email) || 'Email format is not supported'})}
                       type="text"
                       placeholder="Insert Email"
                       value={configuration?.email || ''}
                       onChange={onChange}
                       error={errors.email && errors.email.message}/>
              </div>
            </div>
            <div className="title title-payment">Payment Types Mapping</div>
            <div className="d-flex">
              <div className="input-group">
                <Select {...register('paymentGateway', {required: 'Nothing selected'})}
                        className="form-select"
                        name="paymentGateway"
                        value={configuration?.paymentGateway || ''}
                        onChange={onChange}
                        error={errors.paymentGateway && errors.paymentGateway.message}
                        required>
                  <option className="text-muted" value="">Select Payment Gateway</option>
                  <option value="Paypal">Paypal</option>
                </Select>
              </div>
              <div className="input-group spacer">
                <Select {...register('paymentType', {required: 'Nothing selected'})}
                        className="form-select"
                        name="paymentType"
                        value={configuration?.paymentType || ''}
                        onChange={onChange}
                        error={errors.paymentType && errors.paymentType.message}
                        required>
                  <option className="text-muted" value="">Select Erply Payment Type</option>
                  {paymentType && paymentType.map((type, i) => {
                    return (
                      <option key={type.name + i} value={type.name}>{type.name}</option>
                    )
                  })}
                </Select>
              </div>
            </div>
            <div>
              <button className="btn float-end" onClick={handleSubmit(onSave, onErrors)} disabled={loading}>
                {loading ?
                  <>
                    <span className="spinner-border spinner-border-sm"/> Loading...
                  </>
                  : 'Save Settings'}</button>
            </div>
          </div>
        </div>
        : <span className="spinner-border spinner-border-md position-absolute start-50"/>
      }
    </form>
  )
}

export default ConfigurationForm