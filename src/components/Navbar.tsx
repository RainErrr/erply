import React, {ReactElement, useContext} from 'react'
import {ReactComponent as Logo} from '/src/assets/icons/erply-logo.svg'
import {UserContext} from '../providers/UserProvider'

const Navbar: React.FC = (): ReactElement => {
  const {user, logoutUser} = useContext(UserContext)
  const onLogout = () => logoutUser()

  return (
    <div className="navbar">
      <div className="flex-wrap d-flex h-100 w-100 flex-grow-1 align-items-center">
        <div className="col d-flex justify-content-start">
          <div className="brand">
            <Logo/>
          </div>
          <div className="title">
            <span className="text align-middle">Test Assignment 2021 v1.0</span>
          </div>
        </div>
        <div className="col d-flex justify-content-end">
          {user && <span className="logout align-middle" onClick={onLogout}>Log out</span>}
        </div>
      </div>
    </div>
  )
}

export default Navbar