import {isValidEmail, isValidUrl} from './Validation'

it('is valid email', () => {
  expect(isValidEmail('test@test.com')).toEqual(true)
  expect(isValidEmail('test')).toEqual(false)
  expect(isValidEmail('t@t.t')).toEqual(false)
  expect(isValidEmail('t@t.tt')).toEqual(true)
  expect(isValidEmail('SomeOne@gmail.com')).toEqual(true)
  expect(isValidEmail('hey33@hotmail.ee')).toEqual(true)
  expect(isValidEmail('abcedefghijklmnopqrvswyzxABCDEFGHIJKLMNOPQRVSWYZX1234567890@hotmail.ee')).toEqual(true)
  expect(isValidEmail('test@abcedefghijklmnopqrvswyzxABCDEFGHIJKLMNOPQRVSWYZX1234567890.ee')).toEqual(true)
  expect(isValidEmail('')).toEqual(false)
})

it('is valid url', () => {
  expect(isValidUrl('https://www.test.com')).toEqual(true)
  expect(isValidUrl('http://www.test.com')).toEqual(true)
  expect(isValidUrl('www.test.com')).toEqual(true)
  expect(isValidUrl('test.com')).toEqual(true)
  expect(isValidUrl('test')).toEqual(false)
  expect(isValidUrl('http://www.test.test.com')).toEqual(true)
  expect(isValidUrl('http://www.test.test.com/tests')).toEqual(true)
  expect(isValidUrl('http://www.test.test.com/tests?id=1#$?!@=_-')).toEqual(true)
  expect(isValidUrl('http://www.test.test.com:3000')).toEqual(true)
  expect(isValidUrl('http://255.255.255.255:3000')).toEqual(true)
  expect(isValidUrl('255.255.255.255')).toEqual(true)
  expect(isValidUrl('http://IamInvalid | |')).toEqual(false)
  expect(isValidUrl('')).toEqual(false)
})