import React from 'react'

type ErrorProps = React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> & {
  error: string
}

const Error: React.FunctionComponent<ErrorProps> = ({error, ...props}) => {
  return (
    <div className="invalid-feedback">
      {error}
    </div>
  )
}

export default Error
