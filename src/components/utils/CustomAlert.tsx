import React from 'react'
import {Alert} from '../../providers/AlertProvider'

interface AlertProps {
  alert: Alert
}

const CustomAlert: React.FC<AlertProps> = ({alert}) => {
  return (
    <div className={`alert ${alert.type.toLowerCase()}`}>
      <div className="alert-body">
        {alert.content}
      </div>
    </div>
  )
}

export default CustomAlert