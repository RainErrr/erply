import React from 'react'
import useAlert from '../../hooks/useAlert'
import CustomAlert from './CustomAlert'


const Alerts = () => {
  const {alertContents} = useAlert()

  return (
    <div className="alert-container">
      {alertContents.map(alert => {
        return <CustomAlert alert={alert} key={alert.id}/>
      })}
    </div>
  )
}

export default Alerts