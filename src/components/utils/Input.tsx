import React from 'react'
import Error from './Error'

type InputProps = React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> & {
  error?: string | null
}

const Input: React.ForwardRefExoticComponent<React.PropsWithoutRef<InputProps> & React.RefAttributes<HTMLInputElement>> = React.forwardRef( ({error, ...props}, ref) => {
  return (
    <>
      <input {...props} ref={ref} className={`${props.className} ${error ? 'is-invalid' : ''}`}/>
      {error && <Error error={error}/>}
    </>
  )
})

export default Input
