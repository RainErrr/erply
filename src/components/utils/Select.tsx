import React from 'react'
import Error from './Error'

type SelectProps = React.DetailedHTMLProps<React.SelectHTMLAttributes<HTMLSelectElement>, HTMLSelectElement> & {
  error?: string | null
}

const Select: React.ForwardRefExoticComponent<React.PropsWithoutRef<SelectProps> & React.RefAttributes<HTMLSelectElement>> = React.forwardRef(({error, children, ...props}, ref) => {
  return (
    <>
      <select {...props} ref={ref} className={`${props.className} ${error ? 'is-invalid' : ''}`}>
        {children}
      </select>

      {error && <Error error={error} />}
    </>
  )
})

export default Select
