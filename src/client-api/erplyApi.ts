import api from '../Api'
import {Credentials} from '../components/Login'
import {UserData} from '../providers/UserProvider'
import sessionStorage from '../SessionStorage'

interface Response {
  records: RecordArray[],
  status: {}
}

export type RecordArray = {
  name: string
}

const login = async (credentials: Credentials): Promise<Response> => {
  const data = {...credentials, request: 'verifyUser', sendContentType: '1'}
  return await api.post(`/api/erply/`, data)
}

const getWarehouses = async (user: UserData): Promise<Response> => {
  const sessionKey = sessionStorage.getSessionKey()
  const payload = {clientCode: user.clientCode, sessionKey, request: 'getWarehouses'}
  return await api.post(`/api/erply/`, payload)
}

const getPaymentTypes = async (user: UserData): Promise<Response> => {
  const sessionKey = sessionStorage.getSessionKey()
  const payload = {clientCode: user.clientCode, sessionKey, request: 'getPaymentTypes'}
  return await api.post(`/api/erply/`, payload)
}

export {
  login,
  getWarehouses,
  getPaymentTypes
}
