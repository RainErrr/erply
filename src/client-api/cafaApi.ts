import api from '../Api'
import sessionStorage from '../SessionStorage'


export const saveConfiguration = async (basicForm: any) => {
  const payload = {
    application: 'test-assignment',
    level: 'Warehouse',
    level_id: '1',
    type: 'Test',
    name: 'Front-End Test Assignment 2021',
    value: {...basicForm}
  }
  const jwtToken = sessionStorage.getJwtToken()
  return await api.cafaPost('/api/cafa/configuration', payload, jwtToken)
}

export const getConfiguration = async () => {
  const payload = {
    application: 'test-assignment',
    level: 'Warehouse',
    level_id: '1',
    type: 'Test',
    name: 'Front-End Test Assignment 2021'
  }
  const jwtToken = sessionStorage.getJwtToken()
  return await api.cafaGet(`/api/cafa/configuration`, payload, jwtToken)
}

